const readlineSync = require('readline-sync');
const dictionnaryFunction = require("./dictionnary.js");
const bruteForceFunction = require("./brute_force.js");

async function main(){
    let i = 0;
    let minLength="";
    let maxLength="";

    while (i == 0){
        minLength = readlineSync.question("What is the min length of the password ? ");
        maxLength = readlineSync.question("What is the max length of the password ? ");

        if(!isNaN(minLength) && !isNaN(maxLength)){
            minLength = parseInt(minLength);
            maxLength = parseInt(maxLength);
            if(maxLength > minLength ){
                i++;
            }
        }else{
            console.log("Please select a valuable min and max length");
        }
    }
    while(i != 2){
        let option = readlineSync.question("Choose an option of hack : \n"+
        "1 : Dictionnary attack \n"+" 2 : Brute force attack \n");

        if(!isNaN(option)){
            switch (option) {
                case "1":
                  console.log("You choose the dictionnary attack");
                  await dictionnaryFunction.dictionnary(minLength,maxLength);
                  break;
                case "2":
                  console.log("You choose the brute force attack");
                  let str = "";
                  for(let i=33; i <= 126; i++){
                    str += String.fromCharCode(i);
                  }
                  await bruteForceFunction.bruteForce(str,minLength,maxLength);
                  break;
                default:
                  console.log("Fatal Error");
              }

            i++;
        }else{
            console.log("Please select a valuable option");
        }
        
    }
}

main();
